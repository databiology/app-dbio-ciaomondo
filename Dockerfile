#
#   CIAO Mondo Application Dockerfile
#

# dbio/base:5.0.1 is based on Ubuntu 20.04
FROM dbio/base:5.0.1

# Entrypoint is defined as /usr/local/bin/main.sh in dbio/base:5.0.1
COPY main.sh  /usr/local/bin/main.sh
RUN  chmod +x /usr/local/bin/main.sh
